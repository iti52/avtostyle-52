/**
 * Created on 14.10.16.
 */




jQuery(document).ready(function() {

    if (jQuery(window).width() > 767){
        jQuery(".dropdown-toggle span").html("Услуги");
    }

    if (jQuery('#percent_slider').length){
        jQuery("#percent_slider").ionRangeSlider({
            min: 10,
            max: 70,
            from: 10,
            to: 70,
            step: 1,
            type: 'single',
            postfix: "%",
            grid: true,
            grid_num: 6,
            hide_min_max: false
        });
    }

    if (jQuery('#time_slider').length){
        jQuery("#time_slider").ionRangeSlider({
            min: 6,
            max: 60,
            from: 10,
            to: 20,
            step: 1,
            type: 'single',
            postfix: " мес.",
            grid: true,
            grid_num: 9
        });
    }

    if (jQuery('#vertical').length){
        jQuery('#vertical').lightSlider({
            gallery:true,
            item:1,
            vertical:false,
            thumbItem:5,
            thumbMargin:11,
            slideMargin:0,
            verticalHeight:418,
            vThumbWidth:100
        });
    }

    if (jQuery('#main-carousel').length){
        jQuery('#main-carousel').flickity({
            cellAlign: 'left',
            contain: true,
            pageDots: false,
            arrowShape: {
                x0: 10,
                x1: 60, y1: 50,
                x2: 60, y2: 50,
                x3: 20
            }
        });
    }

    if (jQuery('#news-carousel').length){
        jQuery('#news-carousel').flickity({
            cellAlign: 'left',
            contain: true,
            pageDots: false,
            arrowShape: {
                x0: 10,
                x1: 60, y1: 50,
                x2: 60, y2: 50,
                x3: 20
            }
        });
    }
    $(function () {
        //Datemask dd/mm/yyyy
        jQuery("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        jQuery("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        jQuery("[data-mask]").inputmask();
    });
});





